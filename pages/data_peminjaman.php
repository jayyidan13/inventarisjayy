<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IMS | SMKN 1 Ciomas</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <base href="https://ims-skanic.000webhostapp.com">
  <?php include("../layouts/links.php") ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <!-- Main Header -->
    <?php include("../layouts/header.php");?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Peminjaman
          <small>Inventory Management Software</small>
        </h1>
      </section>
      <!-- Main content -->
      <section class="content container-fluid">
        <div class="box">
          <div class="box-header">
              <a href="#" data-target="#ModalAdd" data-toggle="modal" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Buat Baru</a>
          </div>
          <div class="box-body">
            <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                      <th class="text-center tableNumber">No.</th>
                      <th>Nama Pegawai</th>
                      <th>Tanggal Pinjam</th>
                      <th>Tanggal Kembali</th>
                      <th>Status Peminjaman</th>
                      <th class="text-center tableOpsi">Opsi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    include("../config/config.php");
                    $no=0;
                    $query = mysqli_query($config,"SELECT * FROM table_peminjaman LEFT JOiN table_pegawai ON table_peminjaman.id_pegawai=table_pegawai.id_pegawai") or die (mysqli_error());
                    if (mysqli_num_rows($query) == 0) {
                        echo '<tr><td class="text-center" colspan="6">Tidak ada Data!</td></tr>';
                    }else{
                        while ($data = mysqli_fetch_array($query)) {
                        $no++;
                  ?>
                  <tr>
                      <td class="text-center"><?php echo $no; ?></td>          
                      <td><?php echo $data['nama_pegawai']; ?></td>          
                      <td><?php echo $data['tgl_pinjam']; ?></td>
                      <td><?php echo $data['tgl_kembali']; ?></td>          
                      <td><?php echo $data['status_peminjaman']; ?></td>          
                      <td class="text-center">
                        <a class="label label-primary open_modal" id='<?php echo $data['id_peminjaman']; ?>'><i class="fa fa-edit"></i> Edit</a>
                        <a class="label label-danger" onclick="confirm_modal('pages/crud/proses_hapus_peminjaman.php?&id_peminjaman=<?php echo $data['id_peminjaman']; ?>');"><i class="fa fa-trash"></i> Hapus</a>
                      </td>
                  </tr>
                  <?php
                  }
                  }
                  ?>
                </tbody>  
              </table>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- CRUD MODAL -->
    <!-- Modal Popup untuk Add--> 
    <div id="ModalAdd" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Form - Jenis</h4>
          </div>
          <div class="modal-body">
            <form action="pages/crud/proses_tambah_jenis.php" name="modal_popup" enctype="multipart/form-data" method="POST">
              <div class="form-group">
                <label for="kode_jenis">Kode Jenis</label>
                <input type="text" name="kode_jenis" class="form-control" autocomplete="off" required/>
              </div>
              <div class="form-group">
                <label for="nama_jenis">Nama Jenis</label>
                <input type="text" name="nama_jenis" class="form-control" autocomplete="off" required/>
              </div>
              <div class="form-group">
                <label for="keterangan">Keterangan</label>
                <textarea name="keterangan" class="form-control" autocomplete="off"></textarea>
              </div>
              <div class="modal-footer">
                <button type="reset" class="btn btn-default btn-flat" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Batal</button>
                <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i> Simpan</button>
              </div>
            </form>
          </div>   
        </div>
      </div>
    </div>
    <!-- Modal Popup untuk Edit--> 
    <div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
    <!-- Modal Popup untuk Delete--> 
    <div class="modal fade" id="modal_delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Apa anda yakin ingin menghapusnya?</h4>
          </div>
          <div class="modal-footer" style="padding: 15px !important;">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
            <a class="btn btn-danger btn-flat" id="delete_link"><i class="fa fa-trash"></i> Hapus</a>
          </div>
        </div>
      </div>
    </div>
    <!-- END CRUD MODAL -->
    <!-- Main Footer -->
    <?php include("../layouts/footer.php");?>
  </div>
  <!-- ./wrapper -->
  <?php include("../layouts/scripts.php");?>
  <!-- Javascript untuk popup modal Edit--> 
  <script type="text/javascript">
    $(document).ready(function () {
    $(".open_modal").click(function(e) {
        var m = $(this).attr("id");
        $.ajax({
              url: "pages/crud/form_edit_jenis.php",
              type: "GET",
              data : {id_jenis: m,},
              success: function (ajaxData){
                $("#ModalEdit").html(ajaxData);
                $("#ModalEdit").modal('show',{backdrop: 'true'});
              }
            });
          });
        });
  </script>
  <!-- Javascript untuk popup modal Delete--> 
  <script type="text/javascript">
      function confirm_modal(delete_url)
      {
        $('#modal_delete').modal('show', {backdrop: 'static'});
        document.getElementById('delete_link').setAttribute('url' , delete_url);
      }
      $('#delete_link').click(function(){
        window.location=$('#delete_link')[0].attributes.url.value;
      })
  </script>
</body>
</html>