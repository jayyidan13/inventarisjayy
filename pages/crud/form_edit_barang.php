<?php
    include "../../config/config.php";
	$id_invent=$_GET['id_invent'];
	$modal=mysqli_query($config,"SELECT * FROM table_invent WHERE id_invent='$id_invent'");
	while($r=mysqli_fetch_array($modal)){
?>
<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Edit Barang</h4>
        </div>
        <div class="modal-body">
        	<form action="pages/crud/proses_edit_barang.php" name="modal_popup" enctype="multipart/form-data" method="POST">        		
                <div class="form-group">
                	<label for="kode_barang">Kode Barang</label>
                    <input type="hidden" name="id_invent" class="form-control" value="<?php echo $r['id_invent']; ?>" />
     				<input type="text" name="kode_barang" class="form-control" value="<?php echo $r['kode_barang']; ?>"/>
                </div>
                <div class="form-group">
                	<label for="nama_barang">Nama Barang</label>
     				<input type="text" name="nama_barang" class="form-control" value="<?php echo $r['nama_barang']; ?>"/>
                </div>
                <div class="form-group">
                    <label>Jenis Barang</label>
                    <select name="nama_jenis" class="form-control" required>
                    <option hidden>Pilih Jenis</option>
                    <?php
                        $q=mysqli_query($config,"SELECT * FROM table_jenis");
                        while($show=mysqli_fetch_array($q)){
                    ?>
                    <option <?=($r['id_jenis']==$show['id_jenis'])?'selected':''?> value="<?=$show['id_jenis'];?>"><?=$show['nama_jenis'];?></option>
                    <?php 
                    }
                    ?>
                    </select>
                </div>
                <div class="form-group">
                	<label for="kondisi_barang">Kondisi Barang</label>
     				<input type="text" name="kondisi_barang" class="form-control" value="<?php echo $r['kondisi_barang']; ?>"/>
                </div>
                <div class="form-group">
                    <label>Ruang</label>
                    <select name="nama_ruang" class="form-control" required>
                    <option hidden>Pilih Ruang</option>
                    <?php
                        $q=mysqli_query($config,"SELECT * FROM table_ruang");
                        while($show=mysqli_fetch_array($q)){
                    ?>
                    <option <?=($r['id_ruang']==$show['id_ruang'])?'selected':''?> value="<?=$show['id_ruang'];?>"><?=$show['nama_ruang'];?></option>
                    <?php 
                    }
                    ?>
                    </select>
                </div>
                <div class="form-group">
                	<label for="jumlah_barang">Jumlah Barang</label>
     				<input type="text" name="jumlah_barang" class="form-control" value="<?php echo $r['jumlah_barang']; ?>"/>
                </div>
                <div class="form-group">
                	<label for="tgl_register">Tanggal Register</label>
     				<input type="date" name="tgl_register" class="form-control" value="<?php echo $r['tgl_register']; ?>"/>
                </div>
                <div class="form-group">
                	<label for="keterangan">Keterangan</label>
     				<input type="text" name="keterangan" class="form-control" value="<?php echo $r['keterangan']; ?>">
                </div>
                <div class="form-group">
                    <label>Petugas</label>
                    <select name="nama_petugas" class="form-control">
                    <option hidden>Pilih Petugas</option>
                    <?php
                        $q=mysqli_query($config,"SELECT * FROM table_petugas");
                        while($show=mysqli_fetch_array($q)){
                    ?>
                    <option <?=($r['id_petugas']==$show['id_petugas'])?'selected':''?> value="<?=$show['id_petugas'];?>"><?=$show['nama_petugas'];?></option>
                    <?php 
                    }
                    ?>
                    </select>
                </div>
                <div class="modal-footer">
	                <button type="reset" class="btn btn-default btn-flat" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Batal</button>
	                <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i> Simpan</button>
	            </div>
            </form>
            <?php } ?>
            </div>
        </div>
    </div>
</div>