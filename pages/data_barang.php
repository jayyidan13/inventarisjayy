<?php
  session_start();
  if(empty($_SESSION['level'])){
    echo"<script>window.location.assign('../login.php');</script>";
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>IMS | SMKN 1 Ciomas</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- <base href="https://ims-skanic.000webhostapp.com"> -->
  <base href="http://localhost/Ujikom/">
  <?php include("../layouts/links.php") ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <!-- Main Header -->
    <?php include("../layouts/header.php");?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Inventarisir
          <small>Inventory Management Software</small>
        </h1>
      </section>
      <!-- Main content -->
      <section class="content container-fluid">
      <div class="box">
          <div class="box-header">
              <a href="#" data-target="#ModalAdd" data-toggle="modal" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Buat Baru</a>
          </div>
          <div class="box-body">
              <div class="table-responsive">
                  <table id="example1" class="table table-bordered table-striped table-hover">
                      <thead>
                      <tr>
                          <th class="text-center tableNumber">No.</th>
                          <th>Kode Barang</th>
                          <th>Nama Barang</th>
                          <th>Jenis Barang</th>
                          <th>Kondisi Barang</th>
                          <th>Ruang</th>
                          <th>Jumlah Barang</th>
                          <th>Tanggal Register</th>
                          <th>Keterangan</th>
                          <th>Petugas</th>
                          <th class="text-center tableOpsi">Opsi</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php
                        include("../config/config.php");
                        $no=0;
                        $query = mysqli_query($config,"SELECT table_invent.*,table_jenis.nama_jenis,table_ruang.nama_ruang,table_petugas.nama_petugas FROM table_invent LEFT JOIN table_jenis ON table_invent.id_jenis=table_jenis.id_jenis LEFT JOIN table_ruang ON table_invent.id_ruang=table_ruang.id_ruang LEFT JOIN table_petugas ON table_invent.id_petugas=table_petugas.id_petugas") or die (mysqli_error());
                        if (mysqli_num_rows($query) == 0) {
                            echo '<tr><td class="text-center" colspan="11">Tidak ada Data!</td></tr>';
                        }else{
                            while ($data = mysqli_fetch_array($query)) {
                            $no++;
                      ?>
                      <tr>
                          <td class="text-center"><?php echo $no; ?></td>          
                          <td><?php echo $data['kode_barang']; ?></td>          
                          <td><?php echo $data['nama_barang']; ?></td>
                          <td><?php echo $data['nama_jenis']; ?></td>
                          <td><?php echo $data['kondisi_barang']; ?></td>
                          <td><?php echo $data['nama_ruang']; ?></td>
                          <td><?php echo $data['jumlah_barang']; ?></td>
                          <td><?php echo $data['tgl_register']; ?></td>
                          <td><?php echo $data['keterangan']; ?></td>
                          <td><?php echo $data['nama_petugas']; ?></td>          
                          <td class="text-center">
                            <a class="label label-primary open_modal" id='<?php echo $data['id_invent']; ?>'><i class="fa fa-edit"></i> Edit</a>
                            <a class="label label-danger" onclick="confirm_modal('pages/crud/proses_hapus_barang.php?&id_invent=<?php echo $data['id_invent']; ?>');"><i class="fa fa-trash"></i> Hapus</a>
                          </td>
                      </tr>
                      <?php
                      }
                      }
                      ?>
                    </tbody>  
                  </table>
                </div>
            </div>
          <!-- /.box-body -->
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- CRUD MODAL -->
    <!-- Modal Popup untuk Add--> 
    <div id="ModalAdd" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Form - Barang</h4>
          </div>
          <div class="modal-body">
            <form action="pages/crud/proses_tambah_barang.php" name="modal_popup" enctype="multipart/form-data" method="POST">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="kode_barang">Kode Barang</label>
                  <input type="text" name="kode_barang" class="form-control" autocomplete="off" required/>
                </div>
                <div class="form-group">
                  <label for="nama_barang">Nama Barang</label>
                  <input type="text" name="nama_barang" class="form-control" autocomplete="off" required/>
                </div>
                <div class="form-group">
                  <label for="jumlah_barang">Jumlah Barang</label>
                  <input type="number" name="jumlah_barang" class="form-control" autocomplete="off" required/>
                </div>
                <div class="form-group">
                  <label for="kondisi_barang">Kondisi Barang</label>
                  <input type="text" name="kondisi_barang" class="form-control" autocomplete="off" required/>
                </div>
                <div class="form-group">
                  <label>Jenis Barang</label>
                  <select name="nama_jenis" class="form-control" required>
                    <option selected="selected" hidden disabled="">Pilih Jenis</option>
                    <?php
                      $q=mysqli_query($config,"SELECT * FROM table_jenis ORDER BY nama_jenis ASC");
                      while($show=mysqli_fetch_array($q)){
                    ?>
                    <option value="<?=$show['id_jenis'];?>"><?=$show['nama_jenis'];?></option>
                    <?php 
                    }
                    ?>
                  </select>
                </div>       
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Ruang</label>
                  <select name="nama_ruang" class="form-control" required>
                    <option selected="selected" hidden>Pilih Ruang</option>
                    <?php
                      $q=mysqli_query($config,"SELECT * FROM table_ruang ORDER BY nama_ruang ASC");
                      while($show=mysqli_fetch_array($q)){
                    ?>
                    <option value="<?=$show['id_ruang'];?>"><?=$show['nama_ruang'];?></option>
                    <?php 
                    }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="tgl_register">Tanggal Register</label>
                  <input type="date" name="tgl_register" class="form-control" autocomplete="off" required/>
                </div>
                <div class="form-group">
                  <label for="keterangan">Keterangan</label>
                  <input type="text" name="keterangan" class="form-control" autocomplete="off">
                </div>
                <div class="form-group">
                  <label>Petugas</label>
                  <select name="nama_petugas" class="form-control">
                    <option selected="selected" hidden>Pilih Petugas</option>
                    <?php
                      $q=mysqli_query($config,"SELECT * FROM table_petugas ORDER BY nama_petugas ASC");
                      while($show=mysqli_fetch_array($q)){
                    ?>
                    <option value="<?=$show['id_petugas'];?>"><?=$show['nama_petugas'];?></option>
                    <?php 
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="modal-footer">
                <div class="col-sm-12">
                  <button type="reset" class="btn btn-default btn-flat" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> Batal</button>
                  <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i> Simpan</button>
                </div>
              </div>
            </form>
          </div>   
        </div>
      </div>
    </div>
    <!-- Modal Popup untuk Edit--> 
    <div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
    <!-- Modal Popup untuk Delete--> 
    <div class="modal fade" id="modal_delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Apa anda yakin ingin menghapusnya?</h4>
          </div>
          <div class="modal-footer" style="padding: 15px !important;">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
            <a class="btn btn-danger btn-flat" id="delete_link"><i class="fa fa-trash"></i> Hapus</a>
          </div>
        </div>  
      </div>
    </div>
    <!-- END CRUD MODAL -->
    <!-- Main Footer -->
    <?php include("../layouts/footer.php");?>
  </div>
  <!-- ./wrapper -->
  <?php include("../layouts/scripts.php");?>
  <!-- Javascript untuk popup modal Edit--> 
  <script type="text/javascript">
    $(document).ready(function () {
    $(".open_modal").click(function(e) {
        var m = $(this).attr("id");
        $.ajax({
              url: "pages/crud/form_edit_barang.php",
              type: "GET",
              data : {id_invent: m,},
              success: function (ajaxData){
                $("#ModalEdit").html(ajaxData);
                $("#ModalEdit").modal('show',{backdrop: 'true'});
              }
            });
          });
        });
  </script>
  <!-- Javascript untuk popup modal Delete--> 
  <script type="text/javascript">
      function confirm_modal(delete_url)
      {
        $('#modal_delete').modal('show', {backdrop: 'static'});
        document.getElementById('delete_link').setAttribute('url' , delete_url);
      }
      $('#delete_link').click(function(){
        window.location=$('#delete_link')[0].attributes.url.value;
      })
  </script>
</body>
</html>
<?php 
// if(isset($_GET['with-notification'])){
//   echo"<script>toastr.success('".$_GET['with-notification']."');setTimeout(function(){window.location = removeParams('with-notification')},2000);</script >";
// }
// ?>