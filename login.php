<?php
 session_start();
 include 'config/config.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sarpras | Masuk</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- <base href="https://ims-skanic.000webhostapp.com"> -->
  <base href="http://localhost/Ujikom/">
  <?php include "layouts/links.php";?>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-box-body">
    <div class="login-leftHalf">
      <img src="dist/img/inventory-3.png">
      <p>Inventory Management Software</p>
    </div>
    <div class="login-rightHalf">
      <form action="" method="post">
        <div class="form-group form-group-input has-feedback">
          <input type="text" class="form-control custom-control" name="username" placeholder="Username" autocomplete="off" required>
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group form-group-input has-feedback">
          <input type="password" class="form-control custom-control" name="password" placeholder="Password" autocomplete="off" required>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group form-group">
          Belum terdaftar? <a href="#">Buat akun disini</a>
          <button type="submit" name="login" class="btn btn-login btn-block pull-right"><i class="fa fa-sign-in"></i> Masuk</button>
        </div>
      </form>
    </div>
    <?php
      if (isset($_POST['login'])) {
        $username = $_POST['username'];
        $password = md5($_POST['password']);
        $login = mysqli_query($config,"SELECT * FROM table_petugas where username='$username' and password='$password'");
        $cek = mysqli_num_rows($login);
        if($cek > 0){
          $data = mysqli_fetch_assoc($login);
          if($data['level']=="Admin"){
            $_SESSION['username'] = $username;
            $_SESSION['level'] = "Admin";
            echo"<script>window.location.assign('index.php');</script>";
          }else if($data['level']=="Operator"){
            $_SESSION['username'] = $username;
            $_SESSION['level'] = "Operator";
            echo"<script>window.location.assign('#');</script>";
          }else{
            echo"<script>window.location.assign('login.php');</script>";
          }	
        }else{
          echo"<script>window.location.assign('login.php');</script>";
        }
      }
    ?>
  </div>
</div>
<?php include "layouts/scripts.php";?>
</body>
</html>