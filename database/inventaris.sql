-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2019 at 08:32 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventaris`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_detail`
--

CREATE TABLE `table_detail` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_invent` int(11) NOT NULL,
  `jumlah` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_invent`
--

CREATE TABLE `table_invent` (
  `id_invent` int(11) NOT NULL,
  `kode_barang` varchar(200) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `kondisi_barang` varchar(50) NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `jumlah_barang` varchar(200) NOT NULL,
  `tgl_register` date NOT NULL,
  `keterangan` text NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_jenis`
--

CREATE TABLE `table_jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(25) NOT NULL,
  `kode_jenis` varchar(25) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_jenis`
--

INSERT INTO `table_jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(4, 'Alat Elektronik', 'AL', 'Digunakan untuk keperluan Elektronik'),
(5, 'Alat Olahraga', 'AO', 'Digunakan untuk keperluan Olahraga.'),
(6, 'Alat Tetap', 'AT', 'Tidak dapat digunakan oleh orang lain.');

-- --------------------------------------------------------

--
-- Table structure for table `table_pegawai`
--

CREATE TABLE `table_pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_pegawai`
--

INSERT INTO `table_pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`) VALUES
(1, 'Indra Maulana', '123456789012', 'Jln.HM Syarifuddin No.23 Gg.Damai II RT 01/01'),
(2, 'Amanda Siti Jubaedah', '129312890380', 'Desa Semplak Barat RT 03/02'),
(3, 'Muhammad Rifai', '123912830128', 'Jln.Ciherang Cifor Bogor Barat');

-- --------------------------------------------------------

--
-- Table structure for table `table_peminjaman`
--

CREATE TABLE `table_peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `status_peminjaman` varchar(50) NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_peminjaman`
--

INSERT INTO `table_peminjaman` (`id_peminjaman`, `tgl_pinjam`, `tgl_kembali`, `status_peminjaman`, `id_pegawai`) VALUES
(1, '2019-01-13', '2019-01-14', 'Sedang dipinjam', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_petugas`
--

CREATE TABLE `table_petugas` (
  `id_petugas` int(11) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_petugas`
--

INSERT INTO `table_petugas` (`id_petugas`, `nama_petugas`, `username`, `password`, `level`) VALUES
(9, 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin'),
(10, 'Operator', 'operator', '4b583376b2767b923c3e1da60d10de59', 'Operator');

-- --------------------------------------------------------

--
-- Table structure for table `table_ruang`
--

CREATE TABLE `table_ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(25) NOT NULL,
  `kode_ruang` varchar(25) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_ruang`
--

INSERT INTO `table_ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(9, 'Laboratorium RPL', 'LRPL', 'LAB RPL ini dibagi menjadi dua yaitu LAB RPL 1 dan LAB  RPL 2, yang digunakan untuk kegiatan RPL'),
(10, 'Studio Animasi', 'SANM', 'Studio Animasi ini dibagi menjadi dua kelas yaitu Studio Animasi 1 dan Studio Animasi 2, yang digunakan untuk kegiatan Animasi'),
(11, 'Studio Brodcasting', 'SBC', 'Studio ini ditempatkan dilantai 2, yang digunakan untuk kegiatan Brodcasting'),
(12, 'Bengkel Pengelasan', 'BPL', 'Bengkel ini digunakan untuk kegiatan pengelasan.'),
(13, 'Bengkel Teknik Kendaraan ', 'BTKR', 'Bengkel ini digunakan untuk kegiatan Otomotif.'),
(14, 'Ruang Osis', 'RO', 'Ruang Osis ini berada dilantai 2 didekat tangga. dan digunakan untuk kegiatan Osis'),
(15, 'Ruang TU', 'RTU', 'Berada didekat Lapangan, yang digunakan untuk Administrasi Sekolah'),
(16, 'Ruang Guru', 'RGU', 'Ruang Guru ini ditempatkan disamping Masjid SMKN 1 Ciomas, yang digunakan untuk para Guru.'),
(17, 'Masjid Darul Muttaqin', 'TMA', 'Masjid ini digunakan untuk beribadah Siswa,Guru Beserta Staff lainnya.'),
(18, 'Ruang BK', 'RBK', 'Ruang ini ditempatkan dilantai 2, yang digunakan untuk Guru-guru BK');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `table_detail`
--
ALTER TABLE `table_detail`
  ADD PRIMARY KEY (`id_detail_pinjam`);

--
-- Indexes for table `table_invent`
--
ALTER TABLE `table_invent`
  ADD PRIMARY KEY (`id_invent`);

--
-- Indexes for table `table_jenis`
--
ALTER TABLE `table_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `table_pegawai`
--
ALTER TABLE `table_pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `table_peminjaman`
--
ALTER TABLE `table_peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indexes for table `table_petugas`
--
ALTER TABLE `table_petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `table_ruang`
--
ALTER TABLE `table_ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `table_detail`
--
ALTER TABLE `table_detail`
  MODIFY `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_invent`
--
ALTER TABLE `table_invent`
  MODIFY `id_invent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `table_jenis`
--
ALTER TABLE `table_jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `table_pegawai`
--
ALTER TABLE `table_pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `table_peminjaman`
--
ALTER TABLE `table_peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `table_petugas`
--
ALTER TABLE `table_petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `table_ruang`
--
ALTER TABLE `table_ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
