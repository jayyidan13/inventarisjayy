<?php 
	session_start();
	// cek apakah yang mengakses halaman ini sudah login
	if(empty($_SESSION['level'])){
    echo"<script>window.location.assign('login.php');</script>";
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>sarpras | SMKN 1 Ciomas</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- <base href="https://ims-skanic.000webhostapp.com"> -->
  <base href="http://localhost/Ujikom/">
  <?php include "layouts/links.php";?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <!-- Main Header -->
    <?php include "layouts/header.php";?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard
          <small>Inventory Management Software</small>
        </h1>
      </section>
      <!-- Main content -->
      <section class="content container-fluid">

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <?php include "layouts/footer.php";?>
  </div>
  <!-- ./wrapper -->
  <?php include "layouts/scripts.php";?>
</body>
</html>